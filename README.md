# Шаблон бота Telegram
Шаблон бота Telegram на основе библиотеки
[https://github.com/naltox/telegram-node-bot](https://github.com/naltox/telegram-node-bot)

# Быстрый старт
1. Попросите у @BotFather нового бота
2. Клонируйте этот репозиторий
3. Вставьте токен вашего бота в константу `token` в [src/bot.js](src/bot.js)
4. [Установите Docker](https://docs.docker.com/engine/installation/)
5. [Установите Docker Compose](https://docs.docker.com/compose/install/)
6. Выполните `docker-compose up`
7. Вносите свои правки в [src/bot.js](src/bot.js), бот будет автоматически
перезапускаться при сохранении файла
