/*jslint es6, node, maxlen: 80 */

"use strict";

const token = "";

const tg = require("telegram-node-bot")(token);

tg.router
    .when(["/start"], "StartController")
    .when(["/help"], "HelpController")
    .when(["/ping"], "PingController")
    .when(["/pong"], "PongController")
    .when(["/select"], "SelectController")
    .otherwise("OtherwiseController");

tg.controller("StartController", function greet(context) {
    context.sendMessage("You need /help?");
});

tg.controller("HelpController", function showHelp(context) {
    context.sendMessage(
        `/ping - /pong
/select - selection demo`
    );
});

tg.controller("PingController", function pong(context) {
    context.sendMessage("/pong");
});

tg.controller("PongController", function ping(context) {
    context.sendMessage("/ping");
});

tg.controller("SelectController", function showSelection(context) {
    context.runMenu({
        message: "What do you want?",
        "I want to select something": {
            message: "Really?",
            "yes": function replyToYes(submenuContext) {
                submenuContext.sendMessage("You selected yes");
            },
            "no": function replyToNo(submenuContext) {
                submenuContext.sendMessage("You selected no");
            },
            "anyMatch": function replyToOther(submenuContext) {
                submenuContext.sendMessage(
                    `You selected ${submenuContext.message.text}`
                );
            }
        },
        "anyMatch": function replyToOther(menuContext) {
            menuContext.sendMessage("OKAY ):");
        }
    });
});

tg.controller("OtherwiseController", function replyToUnknownCommand(context) {
    context.sendMessage("Unknown command");
});

console.log("Бот запущен!");
