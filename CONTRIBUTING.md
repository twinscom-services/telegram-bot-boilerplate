# Contributing

1. **Fork**
2. **Clone**

        git clone
3. **Configure remotes**

        git remote add upstream
4. **Branch**

        git checkout -b my-feature-branch -t origin/master
5. **Commit**
6. **Rebase**

        git fetch upstream
        git rebase upstream/master
7. **Test**

        npm test
8. **Push**

        git push origin my-feature-branch
9. **[Open a pull request](https://help.github.com/articles/using-pull-requests/ "Using pull requests · GitHub Help")**

# Code style
The names of the functions should start with a verb: `function doSomething() {`.

The names of the variables and constants should not contain verbs:
`const someConstant`.

A line should not exceed the 80-character limit.

There must be exactly one blank line at the end of every text file.
